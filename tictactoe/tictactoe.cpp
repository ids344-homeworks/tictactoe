// tictactoe.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <stdlib.h>
#include<time.h>
#include <list>

using namespace std;

struct Coordinates;

void InsertToPlayground(string Playground[3][3], Coordinates cord, string toPlace);

int generateRandomNumber(int max, int exclude);

struct Coordinates {
    int Down;
    int Right;
};

//Interface Elements:
void ShowLine(string last, bool hasPlus)
{
    for (int i = 0; i < 3; i++)
    {
        if (i == 0)
            cout << "  " << last;
        else
        {
            if (hasPlus)
                cout << "+";
            else
                cout << "-";
        }

        for (int j = 0; j < 3; j++)
        {
            cout << "-";
        }
    }

    cout << last;

    cout << endl;
}

void ShowPlayground(string Playground[3][3])
{
    for (int i = 0; i < 3; i++)
    {
        if (i == 0)
            ShowLine(".", false);
        else
            ShowLine("|", true);

        cout << "  "; // spacing on left

        cout << "| ";

        for (int j = 0; j < 3; j++)
        {
            cout << Playground[i][j] + " | ";
        }

        cout << endl;
    }

    ShowLine("*", false);
}

//Operations with playground
void FillPlaygroundWithEmptyCell(string Playground[3][3]) {
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            InsertToPlayground(Playground, { i,j }, " ");
        }
    }
};

void FillPossibleControls(string Playground[3][3]) //Llenar el playground
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (Playground[i][j] == " ")
                InsertToPlayground(Playground, { i,j }, to_string(3 * i + j + 1));
        }
    }
}

void InsertToPlayground(string Playground[3][3], Coordinates cord, string toPlace) {

    Playground[cord.Down][cord.Right] = toPlace; //7 - row porque para matriz es desde arriba hacia abajo, para chess es al reves
};

Coordinates GetUserInput(string Playground[3][3], string figureToInsert)
{
    Coordinates cordinates = { -1, -1 };

    string position, fullName;

    do
    {
        cout << "\n\nPress button from 1 to 9 to place " << figureToInsert << endl;
        cin >> position;

        if (position.length() > 1 || position == "0")//Si era introducido el valor �nico(en este caso) , repetir el bucle
        {
            cordinates.Down = -1;
            cout << "\nIt's number between 1 and 9!";
        }
        else
        {
            cordinates.Down = int((position[0] - 49) / 3); // -48 para convertir char a int y -1 mas porque arreglo se empiwza desde 0; /3 nos da renglon
            cordinates.Right = int((position[0] - 49) % 3); //%3 nos da columna

            if (Playground[cordinates.Down][cordinates.Right] == "O" || Playground[cordinates.Down][cordinates.Right] == "X")
            {
                cordinates.Down = -1;
                cout << "\nYou cant place figure here!";
            }
        }

    } while ((cordinates.Down < 0 || cordinates.Down > 2) || (cordinates.Right < 0 || cordinates.Right > 2));

    return cordinates;
}

void InsertFigure(string Playground[3][3], string figureToInsert, Coordinates placeCord)
{
    Coordinates cordinates = { -1, -1 };

    string position, fullName;

    cordinates = placeCord;

    InsertToPlayground(Playground, cordinates, figureToInsert);
}

Coordinates getRandomCordinate(list<Coordinates> _list, int _i) { //asi que listas de c++ no devuelvan elemento por indice
    list<Coordinates>::iterator it = _list.begin();               //Se utiliz� este c�digo para devolver el elemento utilizando indice
   
    for (int i = 0; i < _i; i++) {
        ++it;
    }

    return *it;
}

//Computer coordinate generator

Coordinates GetOptimalPosition(string Playground[3][3], string signToEvaluate, string oppositeSign)
{
    Coordinates cordinates = { -1, -1 };
    string position, fullName;

    for (int i = 0; i < 3; i++) //Columns danger
    {
        if (Playground[i][0] == signToEvaluate && Playground[i][1] == signToEvaluate && Playground[i][2] != oppositeSign)
            cordinates = { i, 2 };
        if (Playground[i][1] == signToEvaluate && Playground[i][2] == signToEvaluate && Playground[i][0] != oppositeSign)
            cordinates = { i, 0 };
        if (Playground[i][0] == signToEvaluate && Playground[i][2] == signToEvaluate && Playground[i][1] != oppositeSign)
            cordinates = { i, 1 };
    }

    for (int i = 0; i < 3; i++) //Renglons danger
    {
        if (Playground[0][i] == signToEvaluate && Playground[1][i] == signToEvaluate && Playground[2][i] != oppositeSign)
            cordinates = { 2, i };
        if (Playground[1][i] == signToEvaluate && Playground[2][i] == signToEvaluate && Playground[0][i] != oppositeSign)
            cordinates = { 0, i };
        if (Playground[0][i] == signToEvaluate && Playground[2][i] == signToEvaluate && Playground[1][i] != oppositeSign)
            cordinates = { 1, i };
    }

    //diagonals 
    if (Playground[0][0] == signToEvaluate && Playground[1][1] == signToEvaluate && Playground[2][2] != oppositeSign)
        cordinates = { 2, 2 };
    if (Playground[1][1] == signToEvaluate && Playground[2][2] == signToEvaluate && Playground[0][0] != oppositeSign)
        cordinates = { 0, 0 };
    if (Playground[0][0] == signToEvaluate && Playground[2][2] == signToEvaluate && Playground[1][1] != oppositeSign)
        cordinates = { 1, 1 };

    if (Playground[0][2] == signToEvaluate && Playground[1][1] == signToEvaluate && Playground[2][0] != oppositeSign)
        cordinates = { 2, 0 };
    if (Playground[1][1] == signToEvaluate && Playground[2][0] == signToEvaluate && Playground[0][2] != oppositeSign)
        cordinates = { 0, 2 };
    if (Playground[0][2] == signToEvaluate && Playground[2][0] == signToEvaluate && Playground[1][1] != oppositeSign)
        cordinates = { 1, 1 };

    return cordinates;
}

Coordinates GetComboPosition(string Playground[3][3], string signToEvaluate, string oppositeSign) //Para predecir  combinacion posible
{
    Coordinates cordinates = { -1, -1 };
    string position, fullName;

    //se utilizan listas para tener conjunto de las coordinadas y luego eligir aleatoriamente
    list<Coordinates> goodCombinations;

    for (int i = 0; i < 3; i++) //Renglon combo
    {
        if (Playground[i][0] != oppositeSign && Playground[i][1] != oppositeSign && Playground[i][2] != oppositeSign)
        {
            if (Playground[i][0] == signToEvaluate)
                goodCombinations.push_front({ i, generateRandomNumber(2,0)} );// combinacion preferible
            if (Playground[i][1] == signToEvaluate)
                goodCombinations.push_front({ i, generateRandomNumber(2,1)} );
            if (Playground[i][2] == signToEvaluate)
                goodCombinations.push_front({ i, generateRandomNumber(2,2)} );
        }

    }

    for (int i = 0; i < 3; i++) //Renglon combo
    {
        if (Playground[0][i] != oppositeSign && Playground[1][i] != oppositeSign && Playground[2][i] != oppositeSign)
        {
            if (Playground[0][i] == signToEvaluate)
                goodCombinations.push_front({ generateRandomNumber(2,0),i} );
            if (Playground[1][i] == signToEvaluate)
                goodCombinations.push_front({ generateRandomNumber(2,1) ,i} );
            if (Playground[2][i] == signToEvaluate)
                goodCombinations.push_front({ generateRandomNumber(2,2), i} );
        }

        if (goodCombinations.size() != 0)
        {
            cordinates = getRandomCordinate(goodCombinations, generateRandomNumber(goodCombinations.size()-1, -1)); //goodCombinations[0];
        }
        else
        {
            cordinates = { -1, -1 };
        }
    }
    goodCombinations.clear();

    return cordinates;
}

Coordinates GetCordForPC(string Playground[3][3], string PCLetter, string playerLetter)
{
    Coordinates cordinates = { -1, -1 };
    string position, fullName;
    list<Coordinates> possibleCombinations;

    cordinates = GetOptimalPosition(Playground, PCLetter, playerLetter); //si puede ganar

    if (cordinates.Down != -1)
        return cordinates;

    cordinates = GetOptimalPosition(Playground, playerLetter, PCLetter); //si el oponente va a ganar

    if (cordinates.Down != -1)
        return cordinates;

    //if no danger

    //if es posible a poner 3 en linea
    cordinates = GetComboPosition(Playground, PCLetter, playerLetter); //Verificar si hay posibilidad de combinacion de 3 signos en linea

    if (cordinates.Down != -1)
        return cordinates;

    //Lo ultimo que se puede hacer
    for (int i = 0; i < 3; i++) //Poner en cualquier lugar disponible
    {
        for (int j = 0; j < 3; j++)
        {
            if (Playground[i][j] != "X" && Playground[i][j] != "O")
                possibleCombinations.push_back({ i, j });
        }
    }
    cordinates = getRandomCordinate(possibleCombinations, generateRandomNumber(possibleCombinations.size() - 1, -1)); 

    if (cordinates.Down != -1)
        return cordinates;

    return cordinates;
}

int generateRandomNumber(int max, int exclude)
{
    int number;
    do
    {
        number = (rand() % (max + 1));

    } while (number == exclude);

    return number;
}

void ClearConsole(int userScore, int computerScore)
{
    // Control of steps =)
    system("cls");

    cout << "--- TIC TAC TOE ---\n\n";
    cout << "User: " << userScore << " | Computer: " << computerScore << endl;;
}

bool IsGameEnded(bool& didPlayerWin, string Playground[3][3]) //verificar si alguien tiene 3 signos en linea
{
    string StreakSign;

    string AuxStreakSign;
    int auxRepeator = 0;
    int auxMaxRepeator = 0;

    //combination in renglon
    for (int i = 0; i < 3; i++) 
    {
        AuxStreakSign = Playground[i][0];
        for (int j = 0; j < 3; j++)
        {
            if (Playground[i][j] == AuxStreakSign)
            {
                auxRepeator++;
            }
        }

        if (auxMaxRepeator < auxRepeator)
        {
            auxMaxRepeator = auxRepeator;

            if (auxMaxRepeator == 3)
            {
                if (AuxStreakSign == "X")
                    didPlayerWin = true;
                else
                    didPlayerWin = false;

                return true;
            }
        }
        auxRepeator = 0;
    }

    //combination in column
    for (int i = 0; i < 3; i++) 
    {
        AuxStreakSign = Playground[0][i];
        for (int j = 0; j < 3; j++)
        {
            if (Playground[j][i] == AuxStreakSign)
            {
                auxRepeator++;
            }
        }

        if (auxMaxRepeator < auxRepeator)
        {
            auxMaxRepeator = auxRepeator;

            if (auxMaxRepeator == 3)
            {
                if (AuxStreakSign == "X")
                    didPlayerWin = true;
                else
                    didPlayerWin = false;

                return true;
            }
        }
        auxRepeator = 0;
    }

    //diagonals
    if ( (Playground[0][0] == Playground[1][1] && Playground[1][1] == Playground[2][2]) || (Playground[0][2] == Playground[1][1] && Playground[1][1] == Playground[2][0]))
    {
        if (Playground[1][1] == "X")
            didPlayerWin = true;
        else
            didPlayerWin = false;

        return true;
    }

    return false;
}

bool IsNotSpaceForTurn(string Playground[3][3]) //Verificar si hay espacio en playground para hacer el turno
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if ( !(Playground[i][j] == "O" || Playground[i][j] == "X") )
                {
                return false;
                }
        }
    }
    return true;
}

void Pause()
{
    cout << "\n";
    system("pause");
}

bool GetPlayerLetter(char playerLetter, bool & isPlayerX)//Ver 
{
    switch (playerLetter)
    {
    case 'x':
        isPlayerX = true;
        return true;
    case 'X':
        isPlayerX = true;
        return true;
    case 'o':
        isPlayerX = false;
        return true;
    case 'O':
        isPlayerX = false;
        return true;
    default:
        return false;
    }
}

int main()
{
    string Playground[3][3];
    bool didPlayerWin = false;
    bool roundFinished = false;
    bool isPlayerX = false;
    char playerLetter=' ';

    int userScore = 0;
    int computerScore = 0;

    FillPlaygroundWithEmptyCell(Playground);
    FillPossibleControls(Playground);

    srand(time(0));
    while (true)
    {
        while (!GetPlayerLetter(playerLetter, isPlayerX))
        {
            ClearConsole(userScore, computerScore);
            cout << "Do you want to be X o O: \n";
            cin >> playerLetter;
        } 

        if (isPlayerX)//Para hacer posible dividir logica de sumar puntos
        {
            //if player is X
            while (!roundFinished)
            {
                didPlayerWin = false;
                ClearConsole(userScore, computerScore);

                ShowPlayground(Playground);

                InsertFigure(Playground, "X", GetUserInput(Playground, "X"));

                //if player won
                if (IsGameEnded(didPlayerWin, Playground))
                {
                    if (didPlayerWin)
                    {
                        userScore++;
                        roundFinished = true;
                        ClearConsole(userScore, computerScore);
                        ShowPlayground(Playground);
                        cout << "\nPlayer won!";
                        Pause();
                        break;
                    }
                    else
                    {
                        computerScore++;
                        roundFinished = true;
                        ClearConsole(userScore, computerScore);
                        ShowPlayground(Playground);
                        cout << "\nComputer won!";
                        Pause();
                        break;
                    }
                    cout << endl;
                }

                //si se acabo el espacio
                if (IsNotSpaceForTurn(Playground))
                {
                    ClearConsole(userScore, computerScore);
                    ShowPlayground(Playground);
                    cout << "\nNobody won!";
                    roundFinished = true;
                    Pause();
                    break;
                }

                InsertFigure(Playground, "O", GetCordForPC(Playground, "O", "X"));

                //If computer won
                if (IsGameEnded(didPlayerWin, Playground) && roundFinished == false)
                {
                    if (didPlayerWin)
                    {
                        userScore++;
                        roundFinished = true;
                        ClearConsole(userScore, computerScore);
                        ShowPlayground(Playground);
                        cout << "\nPlayer won!";
                        Pause();
                        break;
                    }
                    else
                    {
                        computerScore++;
                        roundFinished = true;
                        ClearConsole(userScore, computerScore);
                        ShowPlayground(Playground);
                        cout << "\nComputer won!";
                        Pause();
                        break;
                    }
                    cout << endl;
                }

                //si se acabo el espacio
                if (IsNotSpaceForTurn(Playground))
                {
                    ClearConsole(userScore, computerScore);
                    ShowPlayground(Playground);
                    cout << "\nNobody won!";
                    roundFinished = true;
                    Pause();
                }
            }
        }
        else
        {
            //if player is O
            while (!roundFinished)
            {
                didPlayerWin = false;
                ClearConsole(userScore, computerScore);

                InsertFigure(Playground, "X", GetCordForPC(Playground, "X", "O"));

                ShowPlayground(Playground);

                //if computer won
                if (IsGameEnded(didPlayerWin, Playground))
                {
                    if (didPlayerWin)//En este caso es al reves: si true gan� computador
                    {
                        computerScore++;
                        roundFinished = true;
                        ClearConsole(userScore, computerScore);
                        ShowPlayground(Playground);
                        cout << "\Computer won!";
                        Pause();
                        break;
                    }
                    else
                    {
                        userScore++;
                        roundFinished = true;
                        ClearConsole(userScore, computerScore);
                        ShowPlayground(Playground);
                        cout << "\nPlayer won!";
                        Pause();
                        break;
                    }
                    cout << endl;
                }

                //si se acabo el espacio
                if (IsNotSpaceForTurn(Playground))
                {
                    ClearConsole(userScore, computerScore);
                    ShowPlayground(Playground);
                    cout << "\nNobody won!";
                    roundFinished = true;
                    Pause();
                    break;
                }

                InsertFigure(Playground, "O", GetUserInput(Playground, "O"));

                //If player won
                if (IsGameEnded(didPlayerWin, Playground) && roundFinished == false)
                {
                    if (didPlayerWin)//En este caso es al reves: si true gan� computador
                    {
                        computerScore++;
                        roundFinished = true;
                        ClearConsole(userScore, computerScore);
                        ShowPlayground(Playground);
                        cout << "\Computer won!";
                        Pause();
                        break;
                    }
                    else
                    {
                        userScore++;
                        roundFinished = true;
                        ClearConsole(userScore, computerScore);
                        ShowPlayground(Playground);
                        cout << "\nPlayer won!";
                        Pause();
                        break;
                    }
                    cout << endl;
                }

                //si se acabo el espacio
                if (IsNotSpaceForTurn(Playground))
                {
                    ClearConsole(userScore, computerScore);
                    ShowPlayground(Playground);
                    cout << "\nNobody won!";
                    roundFinished = true;
                    Pause();
                }
            }
        }
        
        playerLetter = ' ';//Para preguntar de nuevo
        FillPlaygroundWithEmptyCell(Playground);
        FillPossibleControls(Playground);
        roundFinished = false;
    }
    return 0;
}
